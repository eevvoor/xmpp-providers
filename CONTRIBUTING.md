# Maintaining the Provider List

This section should help contributors to maintain the [provider list](https://invent.kde.org/melvo/xmpp-providers/blob/master/providers.json).

The provider list is the main resource of this project.
Thus, it should be revised as often as possible and providers must be checked carefully.
We welcome everybody to contribute to this project and encourage you to ask questions in [Kaidan's support group chat](https://i.kaidan.im)!

## Setup

Please follow the steps explained in [Kaidan's basic contribution setup](https://invent.kde.org/network/kaidan/-/wikis/git/setup) if you are unfamiliar with KDE Identity, GitLab or Git.
It should answer most of your questions.
You mostly have to replace `network/kaidan` by `melvo/xmpp-providers` in the text where necessary.

Set up everything needed to work inside of your local repository:
```
cd xmpp-providers
./setup.sh
```

## Rules

Please stick to the following rules:

* Read the [README](/README.md).

### For New Providers

* If a provider has **multiple domains** usable for XMPP, add only the domain that corresponds to the domain of the **provider's website**.
* Send a **test request** to each available support address and if you do not receive a **response within 7 days**, insert `"content": []` and `"comment": "admin@example.org: No response"` (`admin@example.org` must be replaced by the actual address).

### For All Providers

* Have a look at the **existing entries** in the provider list to understand the format and use one of them as a **template for new entries**.
* Provide a **reference** in `source` or set the default value for `content`.
* Only include the **quantity** of units for measurement (e.g., `30` instead of `30 days`) in the provider list.
* **Sort** the entries in the provider list and the commit message in **alphabetically ascending** order by their JIDs.
* As soon as you made all changes to the provider list, save it, add all changes by `git add .` and commit them by `git commit`.
* A Git pre-commit hook is automatically run for identifying **syntax errors** and applying a consistent **format**.
* Use **different commits** for adding, updating and removing entries.
* If you add, update or remove entries, use the following **commit message**: `providers: Add / Update / Remove <added / updated / removed providers>` where `<added / updated / removed providers>` is replaced by the addresses of the providers or by `all providers`, each separated by a comma.

#### Commit Message Examples:

* `providers: Add example.com, example.org`
* `providers: Update example.com, example.net`
* `providers: Update all providers`
* `providers: Remove example.org`
